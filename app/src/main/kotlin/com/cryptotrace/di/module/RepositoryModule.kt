package com.cryptotrace.di.module

import com.cryptotrace.data.repository.CoinRepositoryImpl
import com.cryptotrace.data.repository.HistoryRepositoryImpl
import com.cryptotrace.domain.repository.CoinRepository
import com.cryptotrace.domain.repository.HistoryRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun provideCoinRepository(coinRepository: CoinRepositoryImpl): CoinRepository

    @Binds
    @Singleton
    fun provideHistoryRepository(historyRepository: HistoryRepositoryImpl): HistoryRepository
}
