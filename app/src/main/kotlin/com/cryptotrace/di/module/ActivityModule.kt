package com.cryptotrace.di.module

import com.cryptotrace.di.scope.ActivityScope
import com.cryptotrace.ui.details.DetailsActivity
import com.cryptotrace.ui.details.di.DetailsModule
import com.cryptotrace.ui.home.HomeActivity
import com.cryptotrace.ui.home.di.HomeModule
import com.cryptotrace.ui.search.SearchActivity
import com.cryptotrace.ui.search.di.SearchModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Module(includes = arrayOf(AndroidSupportInjectionModule::class))
interface ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(HomeModule::class))
    fun homeActivityInjector(): HomeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(SearchModule::class))
    fun searchActivityInjector(): SearchActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(DetailsModule::class))
    fun detailsActivityInjector(): DetailsActivity
}