package com.cryptotrace.di.module

import android.app.Application
import android.content.Context
import com.cryptotrace.data.database.AppDatabase
import com.cryptotrace.data.network.RestClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideDatabase(context: Context) = AppDatabase.build(context)

    @Singleton
    @Provides
    fun provideRestClient() = RestClient()
}