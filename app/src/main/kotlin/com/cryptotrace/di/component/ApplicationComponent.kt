package com.cryptotrace.di.component

import android.app.Application
import com.cryptotrace.CryptoTraceApplication
import com.cryptotrace.di.module.ActivityModule
import com.cryptotrace.di.module.ApplicationModule
import com.cryptotrace.di.module.RepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        ApplicationModule::class,
        ActivityModule::class,
        RepositoryModule::class)
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: CryptoTraceApplication)
}
