package com.cryptotrace.ui.search.router

import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.ui.item.CoinItem

interface SearchRouter {

    fun showDetailsScreen(coinItem: CoinItem, coin: Coin)
}