package com.cryptotrace.ui.search.router

import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import com.cryptotrace.R
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.ui.details.DetailsActivity
import com.cryptotrace.ui.item.CoinItem
import com.cryptotrace.ui.search.SearchActivity
import kotlinx.android.synthetic.main.item_coin.view.*
import javax.inject.Inject

class SearchRouterImpl @Inject constructor(private val searchActivity: SearchActivity) : SearchRouter {

    override fun showDetailsScreen(coinItem: CoinItem, coin: Coin) {

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(searchActivity,
                Pair(coinItem.image, searchActivity.getString(R.string.shared_coin_image)),
                Pair(coinItem.name, searchActivity.getString(R.string.shared_coin_name)),
                Pair(coinItem.symbol, searchActivity.getString(R.string.shared_coin_symbol)))

        searchActivity.startActivity(DetailsActivity.getStartedIntent(searchActivity, coin),
                options.toBundle())
    }
}