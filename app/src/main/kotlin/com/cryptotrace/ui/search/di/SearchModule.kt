package com.cryptotrace.ui.search.di

import com.cryptotrace.di.scope.ActivityScope
import com.cryptotrace.ui.search.router.SearchRouter
import com.cryptotrace.ui.search.router.SearchRouterImpl
import dagger.Binds
import dagger.Module

@Module
interface SearchModule {

    @ActivityScope
    @Binds
    fun bindSearchRouter(searchRouterImpl: SearchRouterImpl): SearchRouter
}