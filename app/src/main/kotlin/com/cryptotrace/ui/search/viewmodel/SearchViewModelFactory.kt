package com.cryptotrace.ui.search.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.cryptotrace.domain.interactor.search.CoinListUseCase
import javax.inject.Inject

class SearchViewModelFactory @Inject constructor(private val coinListUseCase: CoinListUseCase) :
        ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(coinListUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}