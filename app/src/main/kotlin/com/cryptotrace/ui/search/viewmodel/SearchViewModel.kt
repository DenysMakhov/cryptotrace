package com.cryptotrace.ui.search.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.domain.interactor.search.CoinListUseCase
import com.cryptotrace.ui.base.viewmodel.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class SearchViewModel constructor(private val coinListUseCase: CoinListUseCase) :
        BaseViewModel(arrayOf(coinListUseCase)) {

    companion object {
        private const val SEARCH_TIMEOUT = 500L
    }

    val loadingStatus = MutableLiveData<Boolean>()
    val response = MutableLiveData<List<Coin>>()
    private val cachedList: MutableList<Coin> = mutableListOf()
    private val filterProcessor: PublishProcessor<String> = PublishProcessor.create<String>()
    private val filterDisposable: Disposable

    init {
        filterDisposable = filterProcessor
                .debounce(SEARCH_TIMEOUT, TimeUnit.MILLISECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { keyword ->
                            if (keyword.isBlank()) {
                                response.value = cachedList
                            } else {
                                val filteredList: MutableList<Coin> = cachedList
                                        .filter { it.fullName.contains(keyword, true) }
                                        .toMutableList()
                                response.value = filteredList
                            }
                        },
                        { it.printStackTrace() }
                )
    }

    fun loadData() {
        loadingStatus.value = true
        coinListUseCase.execute(
                {
                    loadingStatus.value = false
                    cachedList.addAll(it)
                    response.value = cachedList
                },
                {
                    loadingStatus.value = false
                    Log.d("TEST", "onError: $it")
                },
                Unit
        )
    }

    fun filter(keyword: String) {
        filterProcessor.onNext(keyword)
    }

    override fun onCleared() {
        super.onCleared()
        if (!filterDisposable.isDisposed) {
            filterDisposable.dispose()
        }
    }
}