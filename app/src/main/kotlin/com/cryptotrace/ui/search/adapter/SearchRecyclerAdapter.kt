package com.cryptotrace.ui.search.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.ui.base.recycler.OnItemClickListener
import com.cryptotrace.ui.item.CoinItem

class SearchRecyclerAdapter : RecyclerView.Adapter<SearchRecyclerAdapter.DefaultViewHolder>() {

    var coinList: List<Coin>? = null
    var itemClickListener: OnItemClickListener<CoinItem, Coin>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefaultViewHolder {
        return DefaultViewHolder(CoinItem(parent.context))
    }

    override fun getItemCount() = coinList?.size ?: 0

    override fun onBindViewHolder(holder: DefaultViewHolder, position: Int) {
        val itemView = holder.itemView
        coinList?.let {
            if (itemView is CoinItem && coinList != null) {
                itemView.setData(it[position])
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return coinList?.let { it[position].id.toLong() } ?: 0
    }

    inner class DefaultViewHolder(itemView: CoinItem) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                coinList?.let { itemClickListener?.onItemClicked(itemView, it[adapterPosition]) }
            }
        }
    }
}