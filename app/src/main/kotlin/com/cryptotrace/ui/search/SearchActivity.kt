package com.cryptotrace.ui.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import com.cryptotrace.R
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.ui.base.recycler.OnItemClickListener
import com.cryptotrace.ui.item.CoinItem
import com.cryptotrace.ui.search.adapter.SearchRecyclerAdapter
import com.cryptotrace.ui.search.router.SearchRouter
import com.cryptotrace.ui.search.viewmodel.SearchViewModel
import com.cryptotrace.ui.search.viewmodel.SearchViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject


class SearchActivity :
        DaggerAppCompatActivity(),
        OnItemClickListener<CoinItem, Coin> {

    companion object {
        fun getStartedIntent(context: Context) = Intent(context, SearchActivity::class.java)
    }

    @Inject
    lateinit var viewModelFactory: SearchViewModelFactory
    @Inject
    lateinit var router: SearchRouter

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var adapter: SearchRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupRecyclerView()
        setupViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchView = menu.findItem(R.id.search)?.actionView
        if (searchView is SearchView) {
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    searchViewModel.filter(query)
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    searchViewModel.filter(newText)
                    return true
                }
            })
        }
        return true
    }

    private fun setupRecyclerView() {
        adapter = SearchRecyclerAdapter()
        adapter.setHasStableIds(true)
        adapter.itemClickListener = this
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun setupViewModel() {
        searchViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
        searchViewModel.loadingStatus.observe(this, Observer<Boolean> { isLoading ->
            progress.visibility = if (isLoading == true) View.VISIBLE else View.GONE
        })
        searchViewModel.response.observe(this, Observer<List<Coin>> { data ->
            data?.let {
                adapter.coinList = data
                adapter.notifyDataSetChanged()
            }
        })
        searchViewModel.loadData()
    }

    override fun onItemClicked(itemView: CoinItem, data: Coin) {
        router.showDetailsScreen(itemView, data)
    }
}