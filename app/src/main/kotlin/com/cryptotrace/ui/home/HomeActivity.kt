package com.cryptotrace.ui.home

import android.os.Bundle
import com.cryptotrace.R
import com.cryptotrace.ui.search.SearchActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        search.setOnClickListener { startActivity(SearchActivity.getStartedIntent(this)) }
    }
}
