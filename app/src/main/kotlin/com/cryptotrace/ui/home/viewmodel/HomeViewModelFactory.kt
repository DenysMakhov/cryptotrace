package com.cryptotrace.ui.home.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class HomeViewModelFactory constructor(/*private val retrofit: Retrofit*/) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(/*retrofit*/) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}