package com.cryptotrace.ui.home.di

import com.cryptotrace.ui.home.viewmodel.HomeViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class HomeModule {

    @Provides
    fun provideHomeViewModelFactory(/*retrofit: Retrofit*/): HomeViewModelFactory {
        return HomeViewModelFactory(/*retrofit*/)
    }
}