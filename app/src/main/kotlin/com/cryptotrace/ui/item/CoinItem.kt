package com.cryptotrace.ui.item

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.cryptotrace.R
import com.cryptotrace.domain.entity.Coin
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_coin.view.*

class CoinItem(context: Context) : ConstraintLayout(context) {

    init {
        layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        View.inflate(context, R.layout.item_coin, this)
    }

    fun setData(coin: Coin) {
        Picasso.with(context).load(coin.imageUrl).into(image)
        name.text = coin.coinName
        symbol.text = coin.symbol
    }
}