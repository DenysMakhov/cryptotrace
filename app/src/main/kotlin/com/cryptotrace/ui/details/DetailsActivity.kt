package com.cryptotrace.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewCompat
import com.cryptotrace.R
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.domain.entity.HistoryPeriod
import com.cryptotrace.ui.details.viewmodel.DetailsViewModel
import com.cryptotrace.ui.details.viewmodel.DetailsViewModelFactory
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: DetailsViewModelFactory
    private lateinit var coin: Coin
    private lateinit var viewModel: DetailsViewModel

    companion object {

        private const val EXTRA_COIN = "coin"

        fun getStartedIntent(context: Context, coin: Coin): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(EXTRA_COIN, coin)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        coin = intent.getParcelableExtra(EXTRA_COIN)

        ViewCompat.setTransitionName(image, getString(R.string.shared_coin_image))
        ViewCompat.setTransitionName(name, getString(R.string.shared_coin_name))
        ViewCompat.setTransitionName(symbol, getString(R.string.shared_coin_symbol))

        Picasso.with(this).load(coin.imageUrl).into(image)
        name.text = coin.coinName
        symbol.text = coin.symbol

        viewModel = viewModelFactory.create(DetailsViewModel::class.java)
        setupTab()
    }

    private fun setupTab() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    HistoryPeriod.HOUR.ordinal -> viewModel.loadData(coin.symbol, HistoryPeriod.HOUR)
                    HistoryPeriod.DAY.ordinal -> viewModel.loadData(coin.symbol, HistoryPeriod.DAY)
                    HistoryPeriod.WEEK.ordinal -> viewModel.loadData(coin.symbol, HistoryPeriod.WEEK)
                    HistoryPeriod.MONTH.ordinal -> viewModel.loadData(coin.symbol, HistoryPeriod.MONTH)
                    HistoryPeriod.YEAR.ordinal -> viewModel.loadData(coin.symbol, HistoryPeriod.YEAR)
                }
            }
        })
    }
}