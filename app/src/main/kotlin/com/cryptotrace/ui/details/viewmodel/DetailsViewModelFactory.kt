package com.cryptotrace.ui.details.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.cryptotrace.domain.interactor.details.HistoryUseCase
import javax.inject.Inject

class DetailsViewModelFactory @Inject constructor(private val historyUseCase: HistoryUseCase) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(historyUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}