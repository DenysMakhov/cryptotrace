package com.cryptotrace.ui.details.viewmodel

import android.util.Log
import com.cryptotrace.domain.entity.HistoryPeriod
import com.cryptotrace.domain.interactor.details.HistoryUseCase
import com.cryptotrace.ui.base.viewmodel.BaseViewModel

class DetailsViewModel constructor(private val historyUseCase: HistoryUseCase) :
        BaseViewModel(arrayOf(historyUseCase)) {

    fun loadData(coinSymbol: String, period: HistoryPeriod) {
        historyUseCase.execute(
                { Log.d("TEST", it.toString()) },
                { it.printStackTrace() },
                HistoryUseCase.Params(coinSymbol, "USD", period))
    }
}