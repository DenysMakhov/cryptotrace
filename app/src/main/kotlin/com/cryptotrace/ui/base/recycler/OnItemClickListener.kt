package com.cryptotrace.ui.base.recycler

import android.view.View

interface OnItemClickListener<in V : View, in T> {

    fun onItemClicked(itemView: V, data: T)
}