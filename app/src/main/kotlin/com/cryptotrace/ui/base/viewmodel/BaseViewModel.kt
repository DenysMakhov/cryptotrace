package com.cryptotrace.ui.base.viewmodel

import android.arch.lifecycle.ViewModel
import com.cryptotrace.domain.interactor.base.UseCase

abstract class BaseViewModel(useCases: Array<UseCase>) : ViewModel() {

    private var useCaseList: MutableList<UseCase> = mutableListOf()

    init {
        useCaseList.addAll(useCases)
    }

    override fun onCleared() {
        super.onCleared()
        useCaseList.forEach({ it.dispose() })
    }
}