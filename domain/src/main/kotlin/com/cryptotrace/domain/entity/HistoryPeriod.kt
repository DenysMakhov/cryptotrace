package com.cryptotrace.domain.entity

enum class HistoryPeriod {
    HOUR,
    DAY,
    WEEK,
    MONTH,
    YEAR
}