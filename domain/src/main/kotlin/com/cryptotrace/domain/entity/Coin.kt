package com.cryptotrace.domain.entity

import android.os.Parcel
import android.os.Parcelable

data class Coin(
        val id: Int,
        val url: String,
        val imageUrl: String?,
        val name: String,
        val symbol: String,
        val coinName: String,
        val fullName: String,
        val algorithm: String,
        val proofType: String,
        val fullyPremined: Int,
        val totalCoinSupply: Long,
        val preMinedValue: String,
        val totalCoinsFreeFloat: String,
        val sponsored: Boolean,
        val order: Int
) : Parcelable {

    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readLong(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(url)
        writeString(imageUrl)
        writeString(name)
        writeString(symbol)
        writeString(coinName)
        writeString(fullName)
        writeString(algorithm)
        writeString(proofType)
        writeInt(fullyPremined)
        writeLong(totalCoinSupply)
        writeString(preMinedValue)
        writeString(totalCoinsFreeFloat)
        writeInt((if (sponsored) 1 else 0))
        writeInt(order)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Coin> = object : Parcelable.Creator<Coin> {
            override fun createFromParcel(source: Parcel): Coin = Coin(source)
            override fun newArray(size: Int): Array<Coin?> = arrayOfNulls(size)
        }
    }
}