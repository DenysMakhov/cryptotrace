package com.cryptotrace.domain.entity

data class Index(
        val time: Long,
        val open: Float,
        val close: Float,
        val high: Float,
        val low: Float,
        val volumeFrom: Double,
        val volumeTo: Double
)