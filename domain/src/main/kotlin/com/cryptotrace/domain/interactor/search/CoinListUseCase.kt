package com.cryptotrace.domain.interactor.search

import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.domain.interactor.base.SingleUseCase
import com.cryptotrace.domain.repository.CoinRepository
import io.reactivex.Single
import javax.inject.Inject

class CoinListUseCase @Inject constructor(private val coinRepository: CoinRepository) :
        SingleUseCase<List<Coin>, Unit>() {

    override fun buildUseCaseSingle(params: Unit): Single<List<Coin>> = coinRepository.getCoinList()
}