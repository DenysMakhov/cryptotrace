package com.cryptotrace.domain.interactor.details

import com.cryptotrace.domain.entity.HistoryPeriod
import com.cryptotrace.domain.entity.Index
import com.cryptotrace.domain.interactor.base.SingleUseCase
import com.cryptotrace.domain.repository.HistoryRepository
import io.reactivex.Single
import javax.inject.Inject

class HistoryUseCase @Inject constructor(private val historyRepository: HistoryRepository) :
        SingleUseCase<List<Index>, HistoryUseCase.Params>() {

    override fun buildUseCaseSingle(params: Params): Single<List<Index>> {
        return with(params) {
            when(historyPeriod) {
                HistoryPeriod.HOUR -> historyRepository.getHourHistory(fromSymbol, toSymbol)
                HistoryPeriod.DAY -> historyRepository.getDayHistory(fromSymbol, toSymbol)
                HistoryPeriod.WEEK -> historyRepository.getWeekHistory(fromSymbol, toSymbol)
                HistoryPeriod.MONTH -> historyRepository.getMonthHistory(fromSymbol, toSymbol)
                HistoryPeriod.YEAR -> historyRepository.getYearHistory(fromSymbol, toSymbol)
            }
        }
    }

    class Params(
            val fromSymbol: String,
            val toSymbol: String,
            val historyPeriod: HistoryPeriod
    )
}