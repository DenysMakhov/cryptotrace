package com.cryptotrace.domain.repository

import com.cryptotrace.domain.entity.Coin
import io.reactivex.Single

interface CoinRepository {

    fun getCoinList(): Single<List<Coin>>
}