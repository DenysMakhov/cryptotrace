package com.cryptotrace.domain.repository

import com.cryptotrace.domain.entity.Index
import io.reactivex.Single

interface HistoryRepository {

    fun getHourHistory(fromSymbol: String, toSymbol: String): Single<List<Index>>

    fun getDayHistory(fromSymbol: String, toSymbol: String): Single<List<Index>>

    fun getWeekHistory(fromSymbol: String, toSymbol: String): Single<List<Index>>

    fun getMonthHistory(fromSymbol: String, toSymbol: String): Single<List<Index>>

    fun getYearHistory(fromSymbol: String, toSymbol: String): Single<List<Index>>
}