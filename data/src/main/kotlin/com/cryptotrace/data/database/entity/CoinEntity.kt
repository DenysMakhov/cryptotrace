package com.cryptotrace.data.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class CoinEntity(
        @PrimaryKey
        val id: Int,
        val url: String,
        val imageUrl: String?,
        val name: String,
        val symbol: String,
        val coinName: String,
        val fullName: String,
        val algorithm: String,
        val proofType: String,
        val fullyPremined: Int,
        val totalCoinSupply: Long,
        val preMinedValue: String,
        val totalCoinsFreeFloat: String,
        val sponsored: Boolean,
        val sortOrder: Int,
        val cacheTimestamp: Long
)