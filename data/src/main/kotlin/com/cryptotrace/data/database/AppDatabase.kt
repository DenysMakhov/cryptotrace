package com.cryptotrace.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.cryptotrace.data.database.dao.CoinDao
import com.cryptotrace.data.database.entity.CoinEntity

@Database(entities = arrayOf(CoinEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun coinDao(): CoinDao

    companion object {

        fun build(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "App.db")
                        .build()
    }
}