package com.cryptotrace.data.database.mapper

import com.cryptotrace.data.database.entity.CoinEntity
import com.cryptotrace.domain.entity.Coin

class CoinEntityMapper {

    fun toStorage(coin: Coin): CoinEntity {
        val cacheTimestamp = System.currentTimeMillis()
        return with(coin) {
            CoinEntity(
                    coinName = coinName,
                    id = id,
                    url = url,
                    imageUrl = imageUrl,
                    name = name,
                    symbol = symbol,
                    fullName = fullName,
                    algorithm = algorithm,
                    proofType = proofType,
                    fullyPremined = fullyPremined,
                    totalCoinSupply = totalCoinSupply,
                    preMinedValue = preMinedValue,
                    totalCoinsFreeFloat = totalCoinsFreeFloat,
                    sponsored = sponsored,
                    sortOrder = order,
                    cacheTimestamp = cacheTimestamp
            )
        }
    }

    fun fromStorage(coinEntity: CoinEntity): Coin {
        return with(coinEntity) {
            Coin(
                    coinName = coinName,
                    id = id,
                    url = url,
                    imageUrl = imageUrl,
                    name = name,
                    symbol = symbol,
                    fullName = fullName,
                    algorithm = algorithm,
                    proofType = proofType,
                    fullyPremined = fullyPremined,
                    totalCoinSupply = totalCoinSupply,
                    preMinedValue = preMinedValue,
                    totalCoinsFreeFloat = totalCoinsFreeFloat,
                    order = sortOrder,
                    sponsored = sponsored
            )
        }
    }
}