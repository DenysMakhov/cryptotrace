package com.cryptotrace.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.cryptotrace.data.database.entity.CoinEntity
import io.reactivex.Single

@Dao
interface CoinDao {

    @Query("SELECT * FROM CoinEntity ORDER BY sortOrder ASC")
    fun getCoinEntityList(): Single<List<CoinEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCoinEntityList(coinEntityList: List<CoinEntity>)

    @Query("DELETE FROM CoinEntity")
    fun deleteCoinEntityList()
}