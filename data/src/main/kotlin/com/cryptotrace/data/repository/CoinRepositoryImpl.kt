package com.cryptotrace.data.repository

import com.cryptotrace.data.database.AppDatabase
import com.cryptotrace.data.database.mapper.CoinEntityMapper
import com.cryptotrace.data.network.RestClient
import com.cryptotrace.data.network.service.CoinListService
import com.cryptotrace.domain.entity.Coin
import com.cryptotrace.domain.repository.CoinRepository
import io.reactivex.Single
import javax.inject.Inject

class CoinRepositoryImpl @Inject constructor(database: AppDatabase, restClient: RestClient) :
        CoinRepository {

    companion object {
        private const val MAX_CACHE_LIFETIME = 7 * 24 * 60 * 60 * 1000L //WEEK
    }

    private val coinDao = database.coinDao()
    private val coinService = restClient.retrofit.create(CoinListService::class.java)
    private val mapper = CoinEntityMapper()

    override fun getCoinList(): Single<List<Coin>> {

        return getCachedData().flatMap {
            if (it.isEmpty()) {
                getNetworkData()
            } else {
                Single.just(it)
            }
        }
    }

    private fun getCachedData(): Single<List<Coin>> {
        return coinDao.getCoinEntityList()
                .map {
                    val first = it.firstOrNull()
                    if (first != null) {
                        val cacheLifetime = System.currentTimeMillis() - first.cacheTimestamp
                        if (cacheLifetime >= MAX_CACHE_LIFETIME) {
                            coinDao.deleteCoinEntityList()
                            emptyList<Coin>()
                        }
                    }
                    it.map { mapper.fromStorage(it) }
                }
    }

    private fun getNetworkData(): Single<List<Coin>> {
        return coinService.getCoinList()
                .map { it.map() }
                .map {
                    val storageEntities = it.map { mapper.toStorage(it) }
                    coinDao.insertCoinEntityList(storageEntities)
                    it
                }
    }
}