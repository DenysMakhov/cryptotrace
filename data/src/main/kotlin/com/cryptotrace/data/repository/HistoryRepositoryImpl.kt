package com.cryptotrace.data.repository

import com.cryptotrace.data.network.RestClient
import com.cryptotrace.data.network.constant.RequestConstant
import com.cryptotrace.data.network.service.HistoryService
import com.cryptotrace.domain.entity.Index
import com.cryptotrace.domain.repository.HistoryRepository
import io.reactivex.Single
import javax.inject.Inject

class HistoryRepositoryImpl @Inject constructor(restClient: RestClient) : HistoryRepository {

    private val service = restClient.retrofit.create(HistoryService::class.java)

    companion object {
        private val HOUR_LIMIT = "60"
        private val HOUR_AGGREGATE = "1"
        private val DAY_LIMIT = "144"
        private val DAY_AGGREGATE = "10"
        private val WEEK_LIMIT = "168"
        private val WEEK_AGGREGATE = "1"
        private val MONTH_LIMIT = "120"
        private val MONTH_AGGREGATE = "6"
        private val YEAR_LIMIT = "365"
        private val YEAR_AGGREGATE = "1"
    }

    override fun getHourHistory(fromSymbol: String, toSymbol: String): Single<List<Index>> {
        val queryMap = buildQueryMap(fromSymbol, toSymbol, HOUR_LIMIT, HOUR_AGGREGATE)
        return service.getHistoryMinute(queryMap)
                .map { it.map() }
    }

    override fun getDayHistory(fromSymbol: String, toSymbol: String): Single<List<Index>> {
        val queryMap = buildQueryMap(fromSymbol, toSymbol, DAY_LIMIT, DAY_AGGREGATE)
        return service.getHistoryMinute(queryMap)
                .map { it.map() }
    }

    override fun getWeekHistory(fromSymbol: String, toSymbol: String): Single<List<Index>> {
        val queryMap = buildQueryMap(fromSymbol, toSymbol, WEEK_LIMIT, WEEK_AGGREGATE)
        return service.getHistoryHour(queryMap)
                .map { it.map() }
    }

    override fun getMonthHistory(fromSymbol: String, toSymbol: String): Single<List<Index>> {
        val queryMap = buildQueryMap(fromSymbol, toSymbol, MONTH_LIMIT, MONTH_AGGREGATE)
        return service.getHistoryHour(queryMap)
                .map { it.map() }
    }

    override fun getYearHistory(fromSymbol: String, toSymbol: String): Single<List<Index>> {
        val queryMap = buildQueryMap(fromSymbol, toSymbol, YEAR_LIMIT, YEAR_AGGREGATE)
        return service.getHistoryDay(queryMap)
                .map { it.map() }
    }

    private fun buildQueryMap(fromSymbol: String, toSymbol: String, limit: String,
                              aggregate: String): Map<String, String> {
        val queryMap = mutableMapOf<String, String>()
        queryMap.put(RequestConstant.FROM_SYMBOL, fromSymbol)
        queryMap.put(RequestConstant.TO_SYMBOL, toSymbol)
        queryMap.put(RequestConstant.LIMIT, limit)
        queryMap.put(RequestConstant.AGGREGATE, aggregate)
        return queryMap
    }
}