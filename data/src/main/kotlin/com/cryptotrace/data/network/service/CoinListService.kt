package com.cryptotrace.data.network.service

import com.cryptotrace.data.network.mapper.CoinListMapper
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.POST

interface CoinListService {

    @POST("data/all/coinlist")
    fun getCoinList(): Single<CoinListMapper>
}