package com.cryptotrace.data.network.service

import io.reactivex.Single
import retrofit2.http.POST

interface PriceService {

    @POST("/data/price")
    fun getPrice(): Single<String>

    @POST("/data/pricemulti")
    fun getPriceMulti(): Single<String>

    @POST("/data/pricemultifull")
    fun getPriceMultiFull(): Single<String>

    @POST("/data/dayAvg")
    fun getDayAvg(): Single<String>

    @POST("/data/pricehistorical")
    fun getHistoricalPrice(): Single<String>
}