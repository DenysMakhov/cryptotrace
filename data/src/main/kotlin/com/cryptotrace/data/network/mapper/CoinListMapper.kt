package com.cryptotrace.data.network.mapper

import com.cryptotrace.data.network.constant.NetworkConstant
import com.cryptotrace.data.network.constant.ResponseConstant
import com.cryptotrace.domain.entity.Coin
import com.google.gson.annotations.SerializedName

class CoinListMapper(
        @SerializedName(ResponseConstant.DATA)
        val data: HashMap<String, DataObject>) {

    class DataObject(@SerializedName(ResponseConstant.ID)
                     val id: Int,
                     @SerializedName(ResponseConstant.URL)
                     val url: String,
                     @SerializedName(ResponseConstant.IMAGE_URL)
                     val imageUrl: String?,
                     @SerializedName(ResponseConstant.NAME)
                     val name: String,
                     @SerializedName(ResponseConstant.SYMBOL)
                     val symbol: String,
                     @SerializedName(ResponseConstant.COIN_NAME)
                     val coinName: String,
                     @SerializedName(ResponseConstant.FULL_NAME)
                     val fullName: String,
                     @SerializedName(ResponseConstant.ALGORITHM)
                     val algorithm: String,
                     @SerializedName(ResponseConstant.PROOF_TYPE)
                     val proofType: String,
                     @SerializedName(ResponseConstant.FULLY_PREMINED)
                     val fullyPremined: Int,
                     @SerializedName(ResponseConstant.TOTAL_COIN_SUPPLY)
                     val totalCoinSupply: String,
                     @SerializedName(ResponseConstant.PREMINED_VALUE)
                     val preMinedValue: String,
                     @SerializedName(ResponseConstant.TOTAL_COINS_FREE_FLOAT)
                     val totalCoinsFreeFloat: String,
                     @SerializedName(ResponseConstant.SORT_ORDER)
                     val sortOrder: Int,
                     @SerializedName(ResponseConstant.SPONSORED)
                     val sponsored: Boolean
    )

    fun map() = data.values.sortedBy { it.sortOrder }.map { map(it) }

    private fun map(data: DataObject) = with(data) {
        Coin(
                id = id,
                url = url,
                imageUrl = imageUrl?.let { NetworkConstant.IMAGE_BASE_URL + it },
                name = name,
                symbol = symbol,
                coinName = coinName,
                fullName = fullName,
                algorithm = algorithm,
                proofType = proofType,
                fullyPremined = fullyPremined,
                totalCoinSupply = totalCoinSupply.toLongOrNull() ?: 0,
                preMinedValue = preMinedValue,
                totalCoinsFreeFloat = totalCoinsFreeFloat,
                sponsored = sponsored,
                order = sortOrder
        )
    }
}