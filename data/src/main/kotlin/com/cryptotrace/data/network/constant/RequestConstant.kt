package com.cryptotrace.data.network.constant

object RequestConstant {

    const val FROM_SYMBOL = "fsym"
    const val TO_SYMBOL = "tsym"
    const val LIMIT = "limit"
    const val AGGREGATE = "aggregate"
}