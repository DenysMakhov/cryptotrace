package com.cryptotrace.data.network.service

import com.cryptotrace.data.network.mapper.IndexListMapper
import io.reactivex.Single
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface HistoryService {

    @POST("/data/histominute")
    fun getHistoryMinute(@QueryMap options: Map<String, String>): Single<IndexListMapper>

    @POST("/data/histohour")
    fun getHistoryHour(@QueryMap options: Map<String, String>): Single<IndexListMapper>

    @POST("/data/histoday")
    fun getHistoryDay(@QueryMap options: Map<String, String>): Single<IndexListMapper>
}