package com.cryptotrace.data.network.mapper

import com.cryptotrace.data.network.constant.ResponseConstant
import com.cryptotrace.domain.entity.Index
import com.google.gson.annotations.SerializedName

class IndexListMapper(
        @SerializedName(ResponseConstant.DATA)
        val data: List<DataObject>) {

    class DataObject(val time: Long,
                     val open: Float,
                     val close: Float,
                     val high: Float,
                     val low: Float,
                     @SerializedName(ResponseConstant.VOLUME_FROM)
                     val volumeFrom: Double,
                     @SerializedName(ResponseConstant.VOLUME_TO)
                     val volumeTo: Double
    )

    fun map() = data.map { map(it) }

    private fun map(data: DataObject) = with(data) {
        Index(
                time = time,
                open = open,
                close = close,
                high = high,
                low = low,
                volumeFrom = volumeFrom,
                volumeTo = volumeTo
        )
    }
}