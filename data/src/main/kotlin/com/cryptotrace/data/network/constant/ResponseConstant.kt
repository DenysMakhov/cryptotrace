package com.cryptotrace.data.network.constant

object ResponseConstant {

    const val DATA = "Data"
    const val ID = "Id"
    const val URL = "Url"
    const val IMAGE_URL = "ImageUrl"
    const val NAME = "Name"
    const val SYMBOL = "Symbol"
    const val COIN_NAME = "CoinName"
    const val FULL_NAME = "FullName"
    const val ALGORITHM = "Algorithm"
    const val PROOF_TYPE = "ProofType"
    const val FULLY_PREMINED = "FullyPremined"
    const val TOTAL_COIN_SUPPLY = "TotalCoinSupply"
    const val PREMINED_VALUE = "PreMinedValue"
    const val TOTAL_COINS_FREE_FLOAT = "TotalCoinsFreeFloat"
    const val SORT_ORDER = "SortOrder"
    const val SPONSORED = "Sponsored"

    const val VOLUME_FROM = "volumefrom"
    const val VOLUME_TO = "volumeto"
}