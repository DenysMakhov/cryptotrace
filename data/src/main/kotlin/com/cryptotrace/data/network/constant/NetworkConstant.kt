package com.cryptotrace.data.network.constant

object NetworkConstant {

    const val BASE_URL = "https://min-api.cryptocompare.com/"
    const val IMAGE_BASE_URL = "https://www.cryptocompare.com/"
}